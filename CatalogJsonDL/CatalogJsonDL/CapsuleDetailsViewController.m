//
//  CapsuleDetailsViewController.m
//  CatalogJsonDL
//
//  Created by Cristi on 17/11/2016.
//  Copyright © 2016 Cristi. All rights reserved.
//

#import "CapsuleDetailsViewController.h"
#import <UIImageView+AFNetworking.h>

@interface CapsuleDetailsViewController ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@property (nonatomic, strong) NSDictionary *capsuleInfo;

@end

@implementation CapsuleDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.capsuleInfo) {
        [self updateInterfaceWithDetails:self.capsuleInfo];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadCapsuleDetails:(NSDictionary *)details {
    if ([details isKindOfClass:[NSDictionary class]]) {
        self.capsuleInfo = details;
        [self updateInterfaceWithDetails:self.capsuleInfo];
    }
}

- (void)updateInterfaceWithDetails:(NSDictionary *)details {
    NSString *name = [NSString stringWithFormat:@"%@ $%.2f",details[@"name"], [details[@"price"] doubleValue]];
    NSString *image = details[@"img"];
    NSString *taste = details[@"taste"]; // ** not : update from subtitleLabel to <tasteLabel>
    NSString *description = details[@"description"];
    
    self.titleLabel.text = name;
    self.subtitleLabel.text = taste;
    self.descriptionLabel.text = description;
    if ([image isKindOfClass:[NSString class]]) {
        [self.imageView setImageWithURL:[NSURL URLWithString:image]];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
