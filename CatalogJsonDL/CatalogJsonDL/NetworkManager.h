//
//  NetworkManager.h
//  CatalogJsonDL
//
//  Created by Cristi on 17/11/2016.
//  Copyright © 2016 Cristi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CompletionBlock)(NSArray *list, NSError *error);

@interface NetworkManager : NSObject

+ (void)downloadData:(CompletionBlock)completionBlock;

@end
