//
//  ViewController.m
//  CatalogJsonDL
//
//  Created by Cristi on 17/11/2016.
//  Copyright © 2016 Cristi. All rights reserved.
//

#import "CatalogViewController.h"
#import "CapsuleDetailsViewController.h"
#import "NetworkManager.h"

#define KCellIdentifier @"mainMenuCell"
#define kStoryboardDetailsID @"openCapsuleDetails"

@interface CatalogViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *categoriesData;
@property (nonatomic, strong) NSIndexPath *selectedOptionIndex;

@end

@implementation CatalogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    __weak typeof (self) weakSelf = self;
    [NetworkManager downloadData:^(NSArray *list, NSError *error) {
        if ([list isKindOfClass:[NSArray class]]) {
            [weakSelf loadNewCategories:list];
        }
    }];
    
    self.navigationItem.title = @"Catalog";
    self.navigationController.navigationBarHidden = NO;
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Data Management 

- (void)loadNewCategories:(NSArray *)list {
    
    self.categoriesData = [NSArray arrayWithArray:list];
    [self.tableView reloadData];
}

#pragma mark - TableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.categoriesData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *category = self.categoriesData[section];
    if ([category isKindOfClass:[NSDictionary class]]) {
        NSArray *capsules = category[@"capsules"];
        if ([capsules isKindOfClass:[NSArray class]]) {
            return [capsules count];
        }
    }
    return 0;
}

#pragma mark - TableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:KCellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:KCellIdentifier];
    }
    NSDictionary *category = self.categoriesData[indexPath.section];
    NSArray *capsules = category[@"capsules"];
    NSDictionary *capsule = capsules[indexPath.row];
    NSString *name = capsule[@"name"];
    CGFloat price = [capsule[@"price"] doubleValue];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ (%.2f)",name, price];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _selectedOptionIndex = indexPath;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:kStoryboardDetailsID sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kStoryboardDetailsID]) {
        CapsuleDetailsViewController *destination = (CapsuleDetailsViewController *)segue.destinationViewController;
        NSDictionary *category = self.categoriesData[self.selectedOptionIndex.section];
        NSArray *capsules = category[@"capsules"];
        NSDictionary *capsule = capsules[self.selectedOptionIndex.row];
        
        [destination loadCapsuleDetails:capsule];
    }
}

@end
