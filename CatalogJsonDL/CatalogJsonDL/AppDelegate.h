//
//  AppDelegate.h
//  CatalogJsonDL
//
//  Created by Cristi on 17/11/2016.
//  Copyright © 2016 Cristi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (readonly, weak) NSManagedObjectContext *context;

- (void)saveContext;


@end

