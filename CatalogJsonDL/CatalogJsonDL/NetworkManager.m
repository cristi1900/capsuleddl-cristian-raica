//
//  NetworkManager.m
//  CatalogJsonDL
//
//  Created by Cristi on 17/11/2016.
//  Copyright © 2016 Cristi. All rights reserved.
//

#import "NetworkManager.h"
#import <AFNetworking/AFNetworking.h>

#define KJsonLocation @"https://raw.githubusercontent.com/fvessaz/SimpleCatalogBackend/master/catalog.json"

@implementation NetworkManager

+ (void)downloadData:(CompletionBlock)completionBlock {
//    NSURL *url = [NSURL URLWithString:KJsonLocation];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [[manager requestSerializer] setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    
    [manager GET:KJsonLocation parameters:@{} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"DL FINISHED %@",responseObject);
        if ([responseObject isKindOfClass:[NSArray class]]) {
        completionBlock(responseObject, nil);
        } else {
            completionBlock(nil, [NSError errorWithDomain:@"Missing Data" code:-1 userInfo:nil]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionBlock(nil,error);
    }];
}

@end
