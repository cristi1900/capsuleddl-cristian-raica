//
//  Capsule+CoreDataClass.h
//  
//
//  Created by Cristi on 17/11/2016.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Capsule : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Capsule+CoreDataProperties.h"
