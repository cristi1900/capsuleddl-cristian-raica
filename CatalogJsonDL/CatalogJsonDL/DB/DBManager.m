//
//  DBManager.m
//  CatalogJsonDL
//
//  Created by Cristi on 17/11/2016.
//  Copyright © 2016 Cristi. All rights reserved.
//

#import "DBManager.h"
#import "AppDelegate.h"
#import "Category+CoreDataClass.h"
#import "Capsule+CoreDataClass.h"

@implementation DBManager

+ (NSArray *)persistCategoried:(NSArray *)categories {
    NSMutableArray *list = [NSMutableArray array];
    if ([categories isKindOfClass:[NSArray class]]) {
        for (NSDictionary *entry in categories) {
            if ([entry isKindOfClass:[NSDictionary class]]) {
                Category *category = [self addCategory:entry];
                if (category) {
                    [list addObject:category];
                }
            }
        }
    }
    return list;
}

+ (Category *)addCategory:(NSDictionary *)category {
//    NSManagedObjectContext *context = [(AppDelegate*)[UIApplication sharedApplication] context];
    return nil;
}

@end
