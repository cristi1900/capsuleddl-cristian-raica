//
//  Category+CoreDataProperties.m
//  
//
//  Created by Cristi on 17/11/2016.
//
//

#import "Category+CoreDataProperties.h"

@implementation Category (CoreDataProperties)

+ (NSFetchRequest<Category *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Category"];
}

@dynamic intensity;
@dynamic capsules;

@end
