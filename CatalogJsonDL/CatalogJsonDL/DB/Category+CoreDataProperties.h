//
//  Category+CoreDataProperties.h
//  
//
//  Created by Cristi on 17/11/2016.
//
//

#import "Category+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Category (CoreDataProperties)

+ (NSFetchRequest<Category *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *intensity;
@property (nullable, nonatomic, retain) NSManagedObject *capsules;

@end

NS_ASSUME_NONNULL_END
