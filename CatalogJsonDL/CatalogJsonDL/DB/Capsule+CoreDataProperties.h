//
//  Capsule+CoreDataProperties.h
//  
//
//  Created by Cristi on 17/11/2016.
//
//

#import "Capsule+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Capsule (CoreDataProperties)

+ (NSFetchRequest<Capsule *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *categoryID;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) double price;
@property (nullable, nonatomic, copy) NSString *taste;
@property (nullable, nonatomic, copy) NSString *categoryDescription;
@property (nullable, nonatomic, copy) NSString *img;
@property (nullable, nonatomic, retain) Capsule *category;

@end

NS_ASSUME_NONNULL_END
