//
//  DBManager.h
//  CatalogJsonDL
//
//  Created by Cristi on 17/11/2016.
//  Copyright © 2016 Cristi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBManager : NSObject

+ (NSArray *)persistCategoried:(NSArray *)categories;

@end
