//
//  Capsule+CoreDataProperties.m
//  
//
//  Created by Cristi on 17/11/2016.
//
//

#import "Capsule+CoreDataProperties.h"

@implementation Capsule (CoreDataProperties)

+ (NSFetchRequest<Capsule *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Capsule"];
}

@dynamic categoryID;
@dynamic name;
@dynamic price;
@dynamic taste;
@dynamic categoryDescription;
@dynamic img;
@dynamic category;

@end
